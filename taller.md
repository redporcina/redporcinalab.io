---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: RED PORCINA CIAP
subtitle: Propuesta de Taller
hero_height: is-medium
layout: default 
---

# Resumen
Proponemos la realización de un taller para las personas interesadas en colaborar.

# Objetivos
- Generar interés en el proyecto y motivar la participación.
- Capacitar en los aspectos básicos de georeferenciación.
- Sumar capacidad de trabajo.

# Fecha
a definir.

# Duración
dos bloques de 1hr c/u.

# Resultado esperado
Quienes realicen el taller van a poder cargar datos directamenmte a la base de georeferencias.

