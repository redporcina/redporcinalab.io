---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: RED PORCINA CIAP
subtitle: Georeferenciación
hero_height: is-medium
layout: mapa
---

Es un aplicación web libre de producción  colaborativa, que tiene como propósito cooperar en la localización de actores del sistema agroalimentario porcino.