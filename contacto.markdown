---
title: Contacto
hero_height: is-small
layout: default
---

El *Centro de Información de Actividades Porcinas CIAP* es una organización de cooperación interinstitucional e interdisciplinaria  que desde el año 2007 reúne equipos del Instituto Nacional de Tecnología Agropecuaria y  de Universidades  de Argentina y Uruguay que trabajan con usos de tecnologías de información y comunicación para contribuir en la  vinculación de actores,  la generación y difusión de conocimientos orientados a promover el desarrollo sustentable del sistema agroalimentario porcino en países de la región.

Nos podes encontrar en:

Sitio web: [www.ciap.org.ar](www.ciap.org.ar)

Facebok: [infoCIAP](https://www.facebook.com/InfoCIAP/)

Instagram: [ciap_org](https://www.instagram.com/ciap_org/)

Correos: <a href="mailto:redporcina@ciap.org.ar">redporcina@ciap.org.ar</a> / <a href="mailto:info@ciap.org.ar">info@ciap.org.ar</a>

<figure class="image">
    <a href="http://www.ciap.org.ar">
    <img src="/img/logo-ciap.png">
