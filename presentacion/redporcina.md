# Red Porcina CIAP

> Equipo de desarrollo<br><br>
> 05 de junio de 2020



## Agenda

- Características 
- Web app 
- Datos
- Capas
- Altas
- Flujo de datos
- Estadísticas de uso



## Características

- Georeferenciación
- Aplicación web
- [Diseño web adaptable](https://es.wikipedia.org/wiki/Dise%C3%B1o_web_adaptable)
- Sitio web estático
- Ley accesibilidad web Arg
- [Software Libre](https://www.gnu.org/philosophy/free-sw.es.html)
- múltiples usos (además de Red Porcina)



## Web app

- [Jekyll](https://jekyllrb.com)
- [OpenStreetMap](https://www.openstreetmap.org)
- JavaScript
- [Leaflet](https://leafletjs.com)
- "Simple"



## Datos geo ref

- Planilla compartida
- latitud, longitud, ...
- Datos públicos
- Datos reservados



## Capas

- Grupo de instituciones
- Comparten un criterio
- c/hoja -> una capa
- c/capa puede tener +1 color/ícono



## Ej. de una capa

- Instituciones educativas
    - toda la capa con el mismo ícono/color
    - color identifica edu. formal/informal
    - ícono identifica municipales/provinciales



## Altas (1)

1. Acceso restringido
    - Equipo admin
    - Directo
    - db final



## Altas (2)

2. Acceso público
    - Formularios
    - Indirecto
    - db temporal
    - EA: db temp -> db final



## Información

- Mapa
- Buscador



## Flujo

![](img/esquema.png)



## Estadísticas

- [Matomo](https://matomo.org/)
- [https://metricas.ciap.org.ar](https://metricas.ciap.org.a)



## 

Red Porcina CIAP

[https://RedPorcina.ciap.org.ar](https://redporcina.ciap.org.ar)

Presentación

[https://RedPorcina.ciap.org.ar/presentacion](https://redporcina.ciap.org.ar/presentacion)