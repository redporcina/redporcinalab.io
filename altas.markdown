---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: Regístrese
hero_height: is-small
layout: default
---

Te invitamos a registrarte y completar el formulario correspondiente.
¡Sumate a la RED PORCINA CIAP!<br><br>

<div class="buttons are-large">


<a href="/comercios" class="button is-success is-fullwidth">
    Comercios
</a>

<a href="/productores" class="button is-success is-fullwidth">
    Productores
</a>

<a href="/instituciones" class="button is-success is-fullwidth">
    Instituciones
</a>

<a href="/educacion" class="button is-success is-fullwidth">
    Educación
</a>

<a href="/industria" class="button is-success is-fullwidth">
    Industria
</a>

<!-- <a href="/faenaSenasa" class="button is-success ">
    Faena SENASA
</a>

<a href="/faenaNoSenasa" class="button is-success ">
    Faena no SENASA
</a> 

<a href="/acopio" class="button is-success ">
    Acopio
</a> -->

<a href="/asesores" class="button is-success is-fullwidth">
    Asesores y Técnicos
</a>

</div>
