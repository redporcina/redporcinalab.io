---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: Red Porcina CIAP
hero_height: is-small
has-text-justified: true
layout: default
---

El Centro de Información de Actividades Porcinas CIAP es una organización de cooperación interinstitucional e interdisciplinaria  que desde el año 2007 reúne equipos del Instituto Nacional de Tecnología Agropecuaria y  de Universidades  de Argentina y Uruguay que trabajan con usos de tecnologías de información y comunicación para contribuir en la  vinculación de actores,  la generación y difusión de conocimientos orientados a promover el desarrollo sustentable del sistema agroalimentario porcino en países de la región.


RED PORCINA CIAP es un sistema de  georeferenciación libre de producción  colaborativa, que tienen como propósito cooperar en la localización de actores del complejo alimentario porcino.