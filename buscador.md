---
hero_height: is-small
layout: default
title: Buscador
---

<div class="yt-container">
<iframe src="https://archive.org/embed/georef&poster=https://archive.org/download/georef/comobuscar.png" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>
</div><br>

Busca a los actores del sistema agroalimentario porcino: carnicerías, 
educación, industrias, faena y otras opciones como localidad, ciudad o provincia.
<br><br>

<form onsubmit="return false">
    <input id="queryInput" class="input" type="text" placeholder="Text input">
    <button type="submit" onclick="search()">Buscar</button>
</form>

<div id="resultado">

</div>


