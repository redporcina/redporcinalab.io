<script>

  // Set up initial map center and zoom level
  var map = L.map('map', {
    center: [-40.196, -61.853], // EDIT latitude, longitude to re-center map
    zoom: 5,  // EDIT from 1 to 18 -- decrease to zoom out, increase to zoom in
    scrollWheelZoom: true
  })

  // display Carto basemap tiles with light features and labels
  L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, &copy; <a href="https://carto.com/attribution">CARTO</a>'
  }).addTo(map) // EDIT - insert or remove ".addTo(map)" before last semicolon to display by default

  // display Stamen basemap tiles with colored terrain and labels
  L.tileLayer('https://stamen-tiles.a.ssl.fastly.net/terrain/{z}/{x}/{y}.png', {
    attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.'
  })//.addTo(map) // EDIT - insert or remove ".addTo(map)" before last semicolon to display by default

  const createPopup = (feature, ignoreKeys = ['icon', 'color']) => {
    let text = ''
    let properties = feature.properties

    Object.keys(properties).forEach(key => {
      if (properties[key] && !ignoreKeys.includes(key)) {
        text += `<strong>${key}:</strong> ${properties[key]}<br>`
      }
    })
    return text
  }

  const createLayer = (color='red', icon='home') => {
    return L.geoJson(null, {
      pointToLayer: (feature, latlng) => {
        const Marker = L.AwesomeMarkers.icon({
          icon: feature.properties.icon || icon,
          prefix: 'fa',
          markerColor: feature.properties.color || color
        })
        return L.marker(latlng, { icon: Marker })
      },
      onEachFeature: (feature, layer) => {
        layer.bindPopup(createPopup(feature, ['icon', 'color']))
      }
    })
  }

  // upload CSV file from local directory and display latitude and longitude coordinates as default blue markers on map

  d3.csv(dataUrlFaenaNoSenasa )
    .then(data => data.map(ele => {
      ele.lat = +ele.lat
      ele.lng = +ele.lng
      return ele
    }))
    .then(data => { console.log(data)})

  // Instituciones
  const institucionesLayer = createLayer('black')
  
  omnivore.csv(dataUrlInstituciones, null, institucionesLayer)
    .on('error', (e) => { console.log('ERROR Instituciones', e) })
    .addTo(map)

  // comercios
 // const comerciosLayer = createLayer('green')

  //omnivore.csv(dataUrlComercios, null, comerciosLayer)
  //.on('error', (e) => { console.log('ERROR Instituciones', e) })
  //  .addTo(map)

  // educacion
  const educacionLayer = createLayer('blue')

  omnivore.csv(dataUrlEducacion, null, educacionLayer)
  .on('error', (e) => { console.log('ERROR Instituciones', e) })
    .addTo(map)

  // Carnicerias
  const carniceriasLayer = createLayer('green')

  omnivore.csv(dataUrlCarnicerias, null, carniceriasLayer)
  .on('error', (e) => { console.log('ERROR Instituciones', e) })
    .addTo(map)

  // industrias
  const industriasLayer = createLayer('grey')

  omnivore.csv(dataUrlIndustrias, null, industriasLayer)
  .on('error', (e) => { console.log('ERROR Instituciones', e) })
    .addTo(map)

  // FaenaSenasa
  const faenaSenasaLayer = createLayer('orange')

  omnivore.csv(dataUrlFaenaSenasa, null, faenaSenasaLayer)
  .on('error', (e) => { console.log('ERROR Instituciones', e) })
    .addTo(map)

  // faunaNoSenasa
  const faenaNoSenasaLayer = createLayer('green')

  omnivore.csv(dataUrlFaenaNoSenasa, null, faenaNoSenasaLayer)
  .on('error', (e) => { console.log('ERROR FAENA NO SENASA', e) })
    .addTo(map)

  //Productores
  const productoresLayer = createLayer('green')
  
  .on('error', (e) => { console.log('ERROR PRODUCTORES', e) })
    .addTo(map)

  omnivore.csv(dataUrlProductores, null, productoresLayer)
  .on('error', (e) => { console.log('ERROR PRODUCTORES', e) })
    .addTo(map)

/*
  // Acopio
  const acopioLayer = createLayer('green')

  omnivore.csv(dataUrlAcopio, null, acopioLayer)
  .on('error', (e) => { console.log('ERROR ACOPIO', e) })
    .addTo(map)

    // Tecnicos
  const tecnicosLayer = createLayer('green')

  omnivore.csv(dataUrlTecnicos, null, tecnicosLayer)
  .on('error', (e) => { console.log('ERROR TECNICOS', e) })
    .addTo(map)
*/
  const overlays = {
    'Carnicerías': carniceriasLayer,
    'Educación': educacionLayer,
    'Industrias': industriasLayer,
    'Faena Senasa': faenaSenasaLayer,
    'Faena No Senasa': faenaNoSenasaLayer,
    //'Comercios': comerciosLayer,
    'Instituciones': institucionesLayer,
    'Productores': productoresLayer,
    //'Acopio': acopioLayer,
    //'Técnicos y Asesores': tecnicosLayer,
	};
   L.control.layers({}, overlays).addTo(map)

</script>